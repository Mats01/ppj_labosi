package ppj;

import java.util.Scanner;

class SintaksniAnalizator2 {

    static Reader reader = new Reader();

    public static void main(String[] args) {
        System.out.println("Sintaksni analizator starting...");
        // more stuff

        try{
            logic();

        } catch (SintaksniException e){
            System.out.println(e.getMessage());
        }

        reader.close();
    }

    private static void logic() {
        pprint("<program>", 0);
        list(1);
    }

    private static void list(int depth) {
        pprint("<lista_naredbi>", depth);
        if(!reader.hasNext() || reader.getLine().split(" ")[0].equals("KR_AZ")){
            pprint("$",depth+1);
        }
        else if(reader.getLine().split(" ")[0].equals("IDN") || reader.getLine().split(" ")[0].equals("KR_ZA")){
            naredba(depth+1);
            list(depth+1);
        }else{
            throw new SintaksniException("ovo je error");
        }


    }

    private static void naredba(int depth) {
        pprint("<naredba>", depth);
        if(reader.getLine().split(" ")[0].equals("IDN")){
            letsGoIdn(depth+1);
        }
        if(reader.getLine().split(" ")[0].equals("KR_ZA")){
            letsGoForLoop(depth+1);
        }

    }

    private static void letsGoForLoop(int depth) {
        // TODO za petlja
        pprint("<za_petlja>", depth);
        pprint(reader.getLine(), depth+1);

        if(!reader.nextLine().split(" ")[0].equals("IDN")){
            throw new SintaksniException("err " + (reader.getLine().equals("") ? "kraj" : reader.getLine()));

        }
        pprint(reader.getLine(), depth+1);

        if(!reader.nextLine().split(" ")[0].equals("KR_OD")) {
            throw new SintaksniException("err " + (reader.getLine().equals("") ? "kraj" : reader.getLine()));

        }
        pprint(reader.getLine(), depth+1);

        expression(depth+1);

        if(!reader.nextLine().split(" ")[0].equals("KR_DO")) {
            throw new SintaksniException("err " + (reader.getLine().equals("") ? "kraj" : reader.getLine()));

        }
        pprint(reader.getLine(), depth+1);

        expression(depth+1);
        list(depth+1);

        if(!reader.nextLine().split(" ")[0].equals("KR_AZ")) {
            throw new SintaksniException("err " + (reader.getLine().equals("") ? "kraj" : reader.getLine()));

        }
        pprint(reader.getLine(), depth+1);

        reader.nextLine();








        }

    private static void letsGoIdn(int depth) {
        pprint("<naredba_pridruzivanja>", depth);
        pprint(reader.getLine(), depth+1);

        if(!reader.nextLine().split(" ")[0].equals("OP_PRIDRUZI")){
            throw new SintaksniException("err " + (reader.getLine().equals("") ? "kraj" : reader.getLine()));

        }
        pprint(reader.getLine(), depth+1);
        reader.nextLine();
        expression(depth+1);


    }

    private static void expression(int depth) {
        pprint("<E>", depth);
        depth++;
        // T
        TThing(depth);


        EList(depth);

    }

    private static void TThing(int depth) {
        pprint("<T>", depth);
        pridruzivanje(depth+1);
        TList(depth+1);
    }

    private static void EList(int depth) {
        pprint("<E_lista>", depth);
        if(!reader.hasNext()) pprint("$", depth+1);
        else{
            if(reader.getLine().split(" ").equals("OP_PLUS") || reader.getLine().split(" ").equals("OP_MINUS")){
                pprint(reader.getLine(), depth+1);
                reader.hasNext();
                expression(depth+1);
            } else {
                pprint("$", depth+1);
            }
        }
    }

    private static void TList(int depth) {
        pprint("<T_lista>", depth);
        if(!reader.hasNext()) pprint("$", depth+1);
        else{
            if(reader.getLine().split(" ").equals("OP_PUTA") || reader.getLine().split(" ").equals("OP_DIJELI")){
                pprint(reader.getLine(), depth+1);
                reader.hasNext();
                TThing(depth+1);
            } else {
                pprint("$", depth+1);
            }
        }
    }

    private static void pridruzivanje(int depth) {
        pprint("<P>", depth);
        if(!reader.hasNext()) throw new SintaksniException("err kraj");
        pprint(reader.getLine(), depth+1);
        reader.nextLine();
        // TODO sve opcije izraza u izrazu

    }

    private static void pprint(String value, int depth) {
        System.out.printf(" ".repeat(depth) + value + "\n");
    }
}

class Reader {
    String line;
    Scanner input;

    public Reader() {
        this.input = new Scanner(System.in);

        this.line = input.nextLine();
    }

    public String getLine() {
        return line;
    }

    public String nextLine() {
        this.line = input.nextLine();
        return line;
    }

    public boolean hasNext() {
        return input.hasNext();
    }

    public void close(){
        this.input.close();
    }
}

class SintaksniException extends IllegalArgumentException{
    public SintaksniException(String s) {
        super(s);
    }
}
