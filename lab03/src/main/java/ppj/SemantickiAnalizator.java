package ppj;

import java.util.*;

public class SemantickiAnalizator {

    static ReaderSintaksnogUlaza reader = new ReaderSintaksnogUlaza();

    public static void main(String[] args) {
        // more stuff

//        System.out.println(reader.getLine());

        String prevLine = reader.getLine();
        List<Varijabla> varList = new ArrayList<>();
        Varijabla stillBeingDefined = null;
        int currentScope = 0;
        try {
            while (reader.hasNext()) {
                prevLine = reader.getLine();
                reader.nextLine();
                if (reader.getLine().contains("<naredba_pridruzivanja>")) {
                    prevLine = reader.getLine();
                    Varijabla varijabla = new Varijabla(reader.nextLine(), currentScope);
//                    boolean contained = false;
//                    for (var v : varList) {
//                        if (v.reallyEquals(varijabla)) {
//                            contained = true;
//                        }
//                    }
                    if (!varList.contains(varijabla)) {
                        varList.add(0, varijabla);
                    }
                    continue;
                }
                if (reader.getLine().contains("KR_ZA")) {
                    prevLine = reader.getLine();
                    currentScope++;
                    Varijabla varijabla = new Varijabla(reader.nextLine(), currentScope);
                    varList.add(0, varijabla);
                    stillBeingDefined = varijabla;
                    continue;
                }
                if (reader.getLine().contains("<lista_naredbi>")) {
                    prevLine = reader.getLine();
                    reader.nextLine();
                    stillBeingDefined = null;
                    continue;
                }
                if (reader.getLine().contains("KR_AZ")) {

                    int finalScopeIndentation = currentScope;
                    varList.removeIf(v -> {

                        if (v.getScopeIndentation() >= finalScopeIndentation) {
                            return true;
                        }
                        return false;
                    });
                    currentScope--;
                    continue;
                }
                if (reader.getLine().contains("IDN") && !prevLine.contains("<naredba_pridruzivanja>")) {
                    // koristenje varijable
                    Varijabla varijabla = new Varijabla(reader.getLine(), currentScope);
                    if (!varList.contains(varijabla)) {
                        throw new SemantickiException(String.format("err %d %s\n", varijabla.definedAtLine, varijabla.name));
                    }
                    Varijabla defined = null;
                    for (var v : varList) {
                        if (v.equals(varijabla)) {
                            defined = v;

                            if (v.getDefinedAtLine() == varijabla.getDefinedAtLine()) {
                                boolean bad = true;
                                for (var v2 : varList) {
                                    if (v2.equals(varijabla)) {
                                        if(v2.getDefinedAtLine() < varijabla.getDefinedAtLine()){
                                            bad = false;
                                            defined = v2;
                                            break;
                                        }

                                    }

                                }
                                if (bad){
                                    throw new SemantickiException(String.format("err %d %s\n", varijabla.definedAtLine, varijabla.name));

                                }
                            }


                            if (v.getScopeIndentation() > varijabla.getScopeIndentation() || (stillBeingDefined != null && stillBeingDefined.equals(v))) {
                                throw new SemantickiException(String.format("err %d %s\n", varijabla.definedAtLine, varijabla.name));
                            }
                            break;
                        }

                    }
                    System.out.printf("%d %d %s\n", varijabla.definedAtLine, defined.definedAtLine, varijabla.name);

                }


            }
        } catch (SemantickiException e) {
            System.out.printf(e.getMessage());

        }


        reader.close();


    }


    static class Varijabla {
        int scope;
        String name;
        int definedAtLine;

        public Varijabla(String inputLine, int scope) {


            this.scope = scope;
            String[] input = inputLine.split("\\s");
            int length = input.length;
            this.name = String.valueOf(input[length - 1]);
            this.definedAtLine = Integer.parseInt(String.valueOf(input[length - 2]));
        }

        public int getScopeIndentation() {
            return scope;
        }

        public String getName() {
            return name;
        }

        public int getDefinedAtLine() {
            return definedAtLine;
        }

        public boolean reallyEquals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Varijabla varijabla = (Varijabla) o;
            return scope == varijabla.scope && Objects.equals(name, varijabla.name);

        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Varijabla varijabla = (Varijabla) o;
            return Objects.equals(name, varijabla.name);

        }

        @Override
        public int hashCode() {
            return Objects.hash(scope, name);
        }
    }

    static class ReaderSintaksnogUlaza {
        String line;
        Scanner input;

        public ReaderSintaksnogUlaza() {
            this.input = new Scanner(System.in);

            this.line = input.nextLine();
        }

        public String getLine() {
            return line;
        }

        public String nextLine() {
            this.line = input.nextLine();
            return line;
        }

        public boolean hasNext() {
            return input.hasNext();
        }

        public void close() {
            this.input.close();
        }
    }

    static class SemantickiException extends IllegalArgumentException {
        public SemantickiException(String s) {
            super(s);
        }
    }


}
