
import sys
from random import random

class Node(object):

    def __str__(self, depth=0):
        res = (" " * depth) + self.tag
        for child in self.children:

            if child is None:
                continue

            if isinstance(child, Node):
                res += "\n" + child.__str__(depth+1)
            else:
                res += "\n" + (" " * (depth+1)) + str(child)
        return res

    def add(self, obj):
        self.children.append(obj)

    def __init__(self, tag="<program>", id=random()):
        self.tag = tag
        self.children = []
        self.id = id

class NezavrsniZnak(object):
    def __init__(self, znak, id=random()):
        self.znak = znak
        self.id = id
    def set_znak(self, znak):
        self.znak = znak

    def __str__(self):
        return self.znak

class ProdukcijaGramatike:
    def __init__(self, lijevo, desno, primjeni):
        self.lijevo = lijevo
        self.desno = desno
        self.primjeni = primjeni


class Stack:
    def __init__(self):
        self.stack = []

    def push(self, obj):
        self.stack.insert(0, obj)

    def peek(self):
        return self.stack[0]

    def pop(self):
        return self.stack.pop(0)

    def isEmpty(self):
        return self.stack == []

    def __str__(self):
        res = ""
        for i in self.stack:
            res += str(i) + "\n"
        return res[:-1]

    def replace(self, given):
        for i in range(len(given)):
            if(not isinstance(given[i], str)):
                continue
            print(given[i])
            if given[i][0] == ("<"):
                given[i] = Node(given[i])
            else:
                given[i] = NezavrsniZnak(given[i])
            
        
        
        top = self.pop()
    

        to_add = given
        for i in given:
            top.add(i)
        
        # for i in given:
        #     if (isinstance(i, Node)):
        #         to_add.append(i)
        #         top.add(i)

        #     else:
        #         if(i == "$"):
        #             continue
        #         if(citacUlaza.get_curr_line().split(" ")[0] == i ):
        #             top.add(citacUlaza.get_curr_line())
                    
        #             citacUlaza.next_line()
        #         else:
        #             print(f"ocekivano: {i} dobiveno: {citacUlaza.get_curr_line()}")
        #             raise ValueError
        to_add = to_add[::-1]
        for i in to_add:
            self.push(i)

class CitacUlaza:
    def __init__(self):
        self.lines = []
        for line in sys.stdin:
            self.lines.append(line)
        self.lines.append(KRAJ_NIZA)
        self.stashed = None

    def get_curr_line(self):
        return self.lines[0][:-1]

    def next_line(self):
        if self.end():
            return
        self.lines.pop(0)

    def end(self):
        if(len(self.lines) == 1):
            return True
        return False
    
    def skip(self):
        self.stashed = self.get_curr_line()
        self.next_line()
        


def init_grammar():
    global gramatika

    gramatika.append(ProdukcijaGramatike(
        '<program>',
        ['<lista_naredbi>'],
        ['IDN', 'KR_ZA', KRAJ_NIZA]))

    gramatika.append(ProdukcijaGramatike(
        '<lista_naredbi>',
        ['<naredba>', '<lista_naredbi>'],
        ['IDN', 'KR_ZA']))

    gramatika.append(ProdukcijaGramatike(
        '<lista_naredbi>',
        ['$'],
        ['KR_AZ', KRAJ_NIZA]))

    gramatika.append(ProdukcijaGramatike(
        '<naredba>',
        ['<naredba_pridruzivanja>'],
        ['IDN']))

    gramatika.append(ProdukcijaGramatike(
        '<naredba>',
        ['<za_petlja>'],
        ['KR_ZA']))

    gramatika.append(ProdukcijaGramatike(
        '<naredba_pridruzivanja>',
        ['IDN', 'OP_PRIDRUZI', '<E>'],            
        ['IDN']))

    gramatika.append(ProdukcijaGramatike(
        '<za_petlja>',
        ['KR_ZA', 'IDN', 'KR_OD', '<E>', 'KR_DO', '<E>', '<lista_naredbi>', 'KR_AZ'],
        ['KR_ZA']))

    gramatika.append(ProdukcijaGramatike(
        '<E>',
        ['<T>', '<E_lista>'],
        ['IDN', 'BROJ', 'OP_PLUS', 'OP_MINUS', 'L_ZAGRADA']))

    gramatika.append(ProdukcijaGramatike(
        '<E_lista>',
        ['OP_PLUS', '<E>'],
        ['OP_PLUS']))

    gramatika.append(ProdukcijaGramatike(
        '<E_lista>',
        ['OP_MINUS', '<E>'],
        ['OP_MINUS']))

    gramatika.append(ProdukcijaGramatike(
        '<E_lista>',
        ['$',],
        ['IDN', 'KR_ZA', 'KR_DO', 'KR_AZ', 'D_ZAGRADA', KRAJ_NIZA]))

    gramatika.append(ProdukcijaGramatike(
        '<T>',
        ['<P>', '<T_lista>'],
        ['IDN', 'BROJ', 'OP_PLUS', 'OP_MINUS', 'L_ZAGRADA']))

    gramatika.append(ProdukcijaGramatike(
        '<T_lista>',
        ['OP_PUTA',  '<T>'],
        ['OP_PUTA']))

    gramatika.append(ProdukcijaGramatike(
        '<T_lista>',
        ['OP_DIJELI', '<T>'],
        ['OP_DIJELI']))
    gramatika.append(ProdukcijaGramatike(
        '<T_lista>',
        ['$'],
        ['IDN', 'KR_ZA', 'KR_DO', 'KR_AZ', 'OP_PLUS', 'OP_MINUS', 'D_ZAGRADA', KRAJ_NIZA]))

    gramatika.append(ProdukcijaGramatike(
        '<P>',
        ['OP_PLUS', '<P>'],
        ['OP_PLUS']))

    gramatika.append(ProdukcijaGramatike(
        '<P>',
        ['OP_MINUS'],
        ['OP_MINUS']))
    gramatika.append(ProdukcijaGramatike(
        '<P>',
        ['L_ZAGRADA', '<E>', 'D_ZAGRADA'],
        ['L_ZAGRADA']))

    gramatika.append(ProdukcijaGramatike(
        '<P>',
        ['IDN'],
        ['IDN']))

    gramatika.append(ProdukcijaGramatike(
        '<P>',
        ['BROJ'],
        ['BROJ']))


KRAJ_NIZA="@@KRAJ_NIZA"

gramatika=[]

stog=Stack()

citacUlaza=CitacUlaza()

init_grammar()

# trenitni znak = stog.peek()
# tenutni input = u nekoj varijabli /// klasa za ovo?


program=Node(gramatika[0].lijevo)

# odmah na pocetku na stog stavimo pocetni nezavrsni znak ('<program>')
stog.push(program)


while(True):
    for production in gramatika:
        if(isinstance(stog.peek(), Node)):
            if(production.lijevo == stog.peek().tag):
                
                print(f"vrh stoga: {stog.peek().tag }")
                print(f"curr in: {citacUlaza.get_curr_line() }")
                


                if(citacUlaza.get_curr_line().split(" ")[0] in production.primjeni ):

                    stog.replace(production.desno)
        else:
            if(stog.peek().znak == '$'):
                stog.pop()
                continue
            print(f"vrh stoga: {stog.peek().znak }")
            print(f"curr in: {citacUlaza.get_curr_line() }")
            if( citacUlaza.get_curr_line().split(" ")[0] == stog.peek().znak.split(" ")[0] ):
                stog.pop().set_znak(citacUlaza.get_curr_line())
                citacUlaza.next_line()
    
    if(stog.stack == []):
        break
                    


    if(citacUlaza.end()):
        print(stog)
        break


print(program)
